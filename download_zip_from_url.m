url_path = 'C:\Users\jlk-inspection\Downloads\cq500_files.txt';
fileID = fopen(url_path);
urls = textscan(fileID,'%s');
fclose(fileID);

lines = length(urls{1});
save_path = 'D:\_STUDY\_Sample data_CT\';
for l = 330:lines
    thisURL = urls{1}{l};
    urlSplit = split(thisURL, '/');
    if length(urlSplit)>4
        dirName = urlSplit{5}(1:end-4);
        fileName = urlSplit{5};
        disp('start downloading')
        websave([save_path,fileName], thisURL);
        disp('done downloading')
        if fileName(end) == 'p'
            unzip([save_path,fileName],[save_path, dirName]);
            disp('done unzipping');
            delete([save_path,fileName]);
        end
    elseif length(urlSplit) == 4
        websave([save_path,urlSplit{4}], thisURL);
    elseif length(urlSplit) == 3
        websave([save_path,urlSplit{3}], thisURL);
    end
end